﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    private int cantidadPuntos;
    public Text score;
    public GameObject textoFin;
    private bool isPrintText = false;

	// Use this for initialization
	void Start () {
        cantidadPuntos = 0;
	}
	
	// Update is called once per frame
	void Update () {
        
        score.text = "Score : " + cantidadPuntos;
            
	}

    public void subirPuntos() {
        cantidadPuntos++;
    }

    public void finJuego() {
        print("termino el juego");
        textoFin.transform.position = new Vector3(0f,0f,0f);
    }
}
