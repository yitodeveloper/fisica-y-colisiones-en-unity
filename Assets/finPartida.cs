﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class finPartida : MonoBehaviour {
    public GameController controladorJuego;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        controladorJuego.finJuego();

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        controladorJuego.finJuego();
    }
}
