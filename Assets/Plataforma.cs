﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour {
    public GameController controladorJuego;
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        print("Ha ocurrido un evento de collision");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        controladorJuego.subirPuntos();
        Destroy(this.gameObject);
    }
}
